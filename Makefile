.PHONY: clean All

All:
	@echo "----------Building project:[ motuspart3 - Debug ]----------"
	@cd "motuspart3" && "$(MAKE)" -f  "motuspart3.mk"
clean:
	@echo "----------Cleaning project:[ motuspart3 - Debug ]----------"
	@cd "motuspart3" && "$(MAKE)" -f  "motuspart3.mk" clean
