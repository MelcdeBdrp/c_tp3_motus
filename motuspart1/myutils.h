#ifndef MYUTILS
#define MYUTILS

	/**
 * \fn int getStr(char const * s, int lmax)
 * \brief lit une chaine de caractere de taille maximale \a lmax
 * \param \a s la chaine a remplir
 * \param \a lmax le nombre maximum de caracteres lus jusqu'a '\n' inclus. '\n' est remplace par '\0' dans \a s.
 * \return la taille de \a s (sans '\0'), un nombre negatif en cas de probleme comme l'absence de '\n'.
 * \warning \a s doit pouvoir contenir au moins \a lmax caracteres.
 */
int getStr(char * const s, int const lmax) {
	int l=0;
	int c;
	do {
		c = getchar();	/* length is incremented after the copy */
		if(l <lmax-1 || (l==lmax-1 && c == '\n')) {
			s[l++]=c;
		}
	}	while(c != EOF && c != '\n');
	if(l>1)	--l;
	if(s[l] == EOF) { return -2;}		/* length is decremented before the comparison */
	if(s[l] != '\n')	{ return -1;}
	s[l]='\0';
	return l;
}

#endif