/**
 * \file main.c
 * \brief Motus to complete
 * \author Thierry Garaix garaix@emse.fr
 * \version 0.1
 * \date 10 november 2019
 *
 * Program for the game Motus where a secret word has to be found after a limited number of propositions.
 * After each proposition the letters at the same position as in the secret word are kept at their position.
 * Letters present in the proposed word and the secret word but at the wrong position are marked.
 * The game is lost if the word proposed does not exists and/or if the limited number of popositions is exceeded.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h> 
#include "myutils.h"

#define BUFMAX 256		//<! Maximum length read from the instream buffer
#define MATCH '+'
#define ISIN '/'
#define NONE ' '

/**
 * \fn int getWord(char * w)
 * \brief copy a word from \a stdin into \a w
 * \param \a w points to an already allocated char[]
 * \return the number of characters copied to \a w. -1 if an error occured.
 * \warning if the entry is too long an error may be raised.
 */
int getWord(char * w) {
	printf("\nGive a secret word: ");
	
	getStr(w, BUFMAX);
	
	printf("%c[2J", 0x1B);	/* clear the screen */
	return strlen(w);
}

/** 
 * @fn int proposeWord(const char * const secretWord)
 * @brief get a word from \a stdin and compare with the secretWord
 * @param secretWord the word to find out
 * @return the number of differencies
 */
int proposeWord(const char * const secretWord, int wordLenght) {
	printf(" New try: \n");
	char word[wordLenght+1], match[wordLenght+1], matched[wordLenght+1];// match correspond aux match indexé sur le mot proposé, matched indexé sur le mot mystere
	int goodLetter = 0;
	getStr(word, wordLenght+1);

	match[wordLenght] = '\0';
	matched[wordLenght] = '\0';
	
	for(int i = 0; i< wordLenght; i++)
	{
		if(secretWord[i] == word[i])
		{
			goodLetter++;
			match[i] = MATCH;
			matched[i] = MATCH;
		}
		else 
		{
			match[i] = NONE;
			matched[i] = NONE;
		}
	}

	for(int i = 0; i<wordLenght; i++)
	{
		if(match[i] == NONE) 
		{
			for(int j = 0; j<wordLenght; j++)
			{
				if(word[i] == secretWord[j] && matched[j] == NONE)
				{
					match[i] = ISIN;
					matched[j] = ISIN;
				}	
			}
		}
		
	}

	printf("%s  %d\n", match, wordLenght - goodLetter);

	return 	wordLenght - goodLetter;
}


int main(int argc, char **argv)
{
	printf("Start MOTUS\n");
	
	char w[BUFMAX];	/* secret word */
	int len = 0, error=0;
	int maxTry=6;	/* maximum number of tries to find one word */
	int nTry=0;	/* current number of tries */
	int res=-2;	/* success of the words matching */

	len = getWord(w);
	error = len;
	if(error<0)
		perror(" EXIT PROGRAM ");
	
	/* TODO Q3 */ 
	
	
	printf("\n Le mot secret est %s   %d\n",w, len);

	for(nTry = 0; nTry < maxTry && error; nTry++)
	{
		error = proposeWord(w, len);
	}
	if(error)
	{
		printf("\nYou have used all your %d try, the word was %s\n\n", maxTry, w);
	}
	else
	{
		printf("\nYou have found the word '%s' in %d over %d try", w, nTry, maxTry);
	}

	return 0;
}


