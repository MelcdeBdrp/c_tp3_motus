/**

TP_Motus  Melchior de Beaudrap

 * \file motus_lib.c
 * \brief Motus to complete
 *
 * Program for the game Motus where a secret word has to be found after a limited number of propositions.
 * After each proposition the letters at the same position as in the secret word are kept at their position.
 * Letters present in the proposed word and the secret word but at the wrong position are marked.
 * The game is lost if the word proposed does not exists and/or if the limited number of popositions is exceeded.
 */



#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include "motus_lib.h"


const char argMode[] = "--mode"; 
const char argMaxTry[] = "--maxtry"; 
const char argDicoPath[] = "--diconame"; 

const char argModeS[] = "-m"; 
const char argMaxTryS[] = "-t"; 
const char argDicoPathS[] = "-d"; 


int tourMotus(const sParam* params)
{
	char w[BUFMAX];	/* secret word */
	int len = 0;// taille du mot
	int error=0;// nombre d'erreur, 0 si le mot est trouvé
	int nTry=0;	/* current number of tries */
	
	if(params->mode)// en fonction du mode demander un mot ou aller le chercher dans le dico
	{
	
		srand((unsigned) time(NULL));
		do{
			len = getWordDico(params->pathDico, w);
			//printf("len : %d,  [%d;%d]\n", len, params->lMin, params->lMax);
		}while(len < params->lMin || len > params->lMax);  // tant que le mot trové ne respecte pas longueure imposée
		//printf("Word '%s', len %d\n", w, len);
	}
	else
	{
		do{
			len = getWord(w);
			printf("%d, Veuillez entrer un mot de %d a %d caracteres\n", len, params->lMin, params->lMax);
		}while(len < params->lMin || len > params->lMax);  // tant que le mot trové ne respecte pas longueure imposée
		if(!inDico(w, params->pathDico))// si le mot n'est pas dans le dico..
			addDico(w, params->pathDico);// ..l'y ajouter
		if(error<0)
			perror(" EXIT PROGRAM ");
	}
	
	
	printf("\n Le mot secret est %s   %d  (message de dBug in %s:%d)\n",w, len, __FILE__, __LINE__);//dbug

	error = len;
	for(nTry = 0; nTry < params->maxTry && error; nTry++)//laisser l'utilisateur jouer tant qu'il lui reste des coups et qu'il n'a pas trouvé le mot
	{
		error = proposeWord(w, len, params->pathDico);
		if(error == -1)
		{
#if Q10A
			nTry = params->maxTry; // un mot incompatible fait perdre la main
#else			
			nTry --;//  un mot incompatible ne coute pas d'essais
#endif
		}
	}


	if(error)// s'il reste des erreurs le mot n'a pas été trouvé
	{
		printf("\nVous avez utilisé vos %d essais, le mot était '%s'\n\n", params->maxTry, w);
		return 0;
	}
	else
	{
		printf("\nVous avez trouvé le mot '%s' en %d coups sur %d\n\n", w, nTry, params->maxTry);
		return 1;
	}


}

//jouer une partie
void playMotus(sParam* params)
{

	int endG = 0;
	int player = 0;// equipe qui à la main
	

			//initialisation de la partie
			//
	printf("\nEntrez le nom de l'equipe 1 :");
	getStr(params->teams[0].name, 99);
	
	printf("\nEntrez le nom de l'equipe 2 :");
	getStr(params->teams[1].name, 99);

	initTeam(params->teams, params->maxTry);
	initTeam(params->teams+1, params->maxTry);
	
	if(params->lMin>params->lMax)//verification de securité (boucle infinie en cas d'inversion des valeurs)
	{
		int tmp = params->lMin;
		params->lMin = params->lMax;
		params->lMax = tmp;
	}	

	//printf("\nLa partie dure %d tours et oppose les equipes : %s et %s\n", params->nbTours, params->teams[0].name, params->teams[1].name);
	printf("\nLa partie oppose les equipes : %s et %s\n", params->teams[0].name, params->teams[1].name);
	if(params->mode)	printf("Par tours, vous avez %d essais pour trouver le mot sellectionné dans le dictionnaire%s\n\n", params->maxTry, params->pathDico);
	else	printf("Par tours, vous avez %d essais pour trouver le mot choisis par votre adversaire\n\n", params->maxTry);

	


	//printf("Debut : %d tours\n", params->nbTours);
	while(!endG)		// tant qu'aucune equipe ai gagné
	{
		printf("\n\n C'est a '%s' de jouer", params->teams[player].name);
		int win = tourMotus(params);// jouer un tour
		if(win)
		{
			pickBall(params->teams+player); // params->teams est un tableau et player l'index ..
			printMat(params->teams+player);//..  params->teams+player <=> &(params->teams[player]) 
			endG = victoire(params->teams+player);
			if(endG)
			{
				printf("VICTOIRE \n");
			}
			params->teams[player].toursJ++;
			params->teams[player].toursG++;
		}
		else
		{
			params->teams[player].toursJ++;
			player = !player; // donner la main à l'autre equipe
		}
		printf("\n\n %s j:%d g:%d,  %s j:%d, g:%d", params->teams[0].name, params->teams[0].toursJ, params->teams[0].toursG, params->teams[1].name, params->teams[1].toursJ, params->teams[1].toursG);
	}

}

int victoire(sTeam* team)// retrourne 1 si un collone, une ligne ou une diagonnale ne contient que des billes noires
{
	int line = 0;
	int col = 0; 
	int diag = 0, backdiag = 0;
	int win = 0;


	for(int i = 0; i<team->tMat; i++)
	{
		diag += team->mat[i][team->tMat-1-i];// additionner toutes les cases de la diagonnale j = -i + (tMat-1)
		backdiag += team->mat[i][i];// additionner toutes les cases de la diagonnale j = i 
		for(int j = 0; j<team->tMat; j++)
		{
			col += team->mat[j][i];// additionner toutes les cases de la colone i
			line += team->mat[i][j];	// additionner toutes les cases de la ligne i
		}
		if(col == 0 || line == 0)win = 1;//si la somme est nulle, il y a que de boulles noires, l'équipe à gagné

		col = 0; line = 0;
	}
	if(diag == 0 || backdiag == 0)win = 1;//si la somme est nulle, il y a que de boulles noires, l'équipe à gagné
	return win;
}


int pickBall(sTeam* team) // tire un bille alléatoirement et la place dans la matrice (selon les regles imposées)
{
	int tirage = rand()%(team->nbNc + team->nbNoires);
	printf("Tirage : %d, billes Nc : %d", tirage, team->nbNc);
	if(tirage > team->nbNc) return 0;
	else
	{
		int cpt = 0;
		for(int i = 0; i< team->tMat; i++)//parcourrire la matrice
		{
			for(int j = 0; j< team->tMat; j++)
			{
				if(team->mat[i][j] == 1)// si la case n'est pas recouverte,
				{
					if(cpt >= tirage)//on place la bille si on est a la 'tirage'ième place
					{
						team->mat[i][j] = 0;
						team->nbNc --;
						team->nbNoires ++;
						i = team->tMat;
						j = team->tMat;
					}
					else  cpt++;// si non on augmente cpt(compteur de cases vides)
				}
			}
		}
		return 1;

	}
}

void initTeam(sTeam* team, int tMat)// initialise la structure team, la matrice, les nobre de boulles et les nombres de tours
{
	team->toursJ = 0;
	team->toursG = 0;
	team->tMat = tMat;
	int nbCase = tMat*tMat;
	double cn = (double)nbCase*0.05;
	int cnr = (int)cn;
	if(cn>cnr) cnr += 1;
	team->nbNoires = (int)cnr;
	team->nbNc = nbCase;

	//allocation et initialisation de la matrice de taille tMat
	team->mat = malloc(sizeof(int*)*tMat);
	for(int i = 0; i<tMat; i++)
	{
		team->mat[i] = malloc(sizeof(int)*tMat);
		for(int j = 0; j<tMat; j++)
		{
			team->mat[i][j] = 1;
		}
	}
}

void printMat(sTeam* team) // afficher la matrice de boulles à l'ecran
{
	printf("\n");
	for(int i = 0; i<team->tMat; i++)
	{
		for(int j = 0; j<team->tMat; j++)
		{
			printf("%d ",team->mat[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}



/**
 * \fn int getWord(char * w)
 * \brief copy a word from \a stdin into \a w
 * \param \a w points to an already allocated char[]
 * \return the number of characters copied to \a w. -1 if an error occured.
 * \warning if the entry is too long an error may be raised.
 */
int getWord(char * w) // recuperer un mot au pres de l'utilisateur l'enregidter dans la chaine passée en paremètres et renvoyer la logueure de celle-ci
{  
	printf("\nGive a secret word: ");
	
	getStr(w, BUFMAX);
	int len = strlen(w);
	lowerCase(w, len);
	printf("%c[2J", 0x1B);	/* clear the screen */
	return len;
}


/** 
 * @fn int inDico(const char* const word, const char * const dicFileName)
 * @brief check if a word is in a dictionnary
 * @param \a word the word to find
 * @param \a dicFileName the name of the file
 * @return 1 if the word is in the file and 0 otherwise
 */
int inDico(const char* word, const char * dico) 
{
	FILE* file = NULL;
	//unsigned long tailleDico = 0;
	file = fopen(dico, "r");
	int isInDico = 0;

	if (file != NULL)// s'assurer que le fichier à bien été ouvert
	{
		rewind(file);//se placer au debut du fichier

		char w[BUFMAX];	
		while(fgets(w, BUFMAX-1, file) && !isInDico)// parcourrir toutes les lignes du fichier tant qu'on a pas trouvé le mot recherché
		{
			int lenTmp = strlen(w);
			for(int i = 0; i<lenTmp;i++)//normaliser la chaine pour avoir uniquement le mot terminé par un \0  (fin de chaine)
			{
				if(w[i] == '\n' || w[i] == '\r' || w[i] == '\0')
				{
					w[i] = '\0';
					i = lenTmp;
				}
			}
			if(!strcmp(w, word))// comparer le mot cherché avec celui extrait du fichier
			{
				isInDico = 1;
			}
		}
		
		//lenTmp = strlen(w);
		//printf("|%s| len %d",w, lenTmp);

		fclose(file); // On ferme le fichier qui a été ouvert
		return isInDico;
	}
	else 
	{
		printf("Impossible d'ouvrir le fichier : %s\n", dico);
		return -1;
	}
	

	return 0;	
}


/** 
 * @fn int proposeWord(const char * secretWord, int wordLength, const char* dico) 
 * @brief get a word from \a stdin and compare with the secretWord
 * @param secretWord the word to find out
 * @return the number of differencies
 */
//demander à l'utilisateur de rentrer un mot, verifier qu'il soit valide(dans le dico et de la bonne longeurs)
//comparer le mot secret et indique à l'utilisateur lettres bien placées et mal placées
//retourner le nombre de lettres bien placées manquantes
int proposeWord(const char * secretWord, int wordLength, const char* dico) 
{
	char word[wordLength+3];//mot entré par le joueur
	char match[wordLength+1];// chaine contenant les '+' '/' et ' ' affichée au joueur
	char matched[wordLength+1];// comme la match mais indexé sur le mot mystere
	int goodLetter = 0;//compteur de lettre bien placées
	int isInDico = 0, isRightSize = 0;//boolens
	
	printf("\nNew try:\t");//demander une proposition
	getStr(word, wordLength+2);
	lowerCase(word,wordLength);//normaliser la case (simplification du traitement ne prenalise pas l'usage de majuscules)
	
	isInDico = inDico(word, dico);//verifier que le mots soit dans le dico
	if(strlen(word) == wordLength)isRightSize = 1;//verifier qu'il soit de la bonne longueurs
	
	match[wordLength] = '\0';//delimiter les fin de chaine
	matched[wordLength] = '\0';

	if(isInDico && isRightSize)
	{
			// verifier les lettres placées au bon endroit
		for(int i = 0; i< wordLength; i++)
		{
			if(secretWord[i] == word[i])
			{
				goodLetter++;
				match[i] = MATCH;
				matched[i] = MATCH;
			}
			else 
			{
				match[i] = NONE;
				matched[i] = NONE;
			}
		}

		for(int i = 0; i<wordLength; i++)//parcourrir word 
		{
			if(match[i] == NONE) //verifier que la lettre de "word" n'ai pas dejà ete maché precedement
			{
				for(int j = 0; j<wordLength; j++) // parcourrir secretWord
				{
						//regarder si la lettre existe dans secretWord
						//et verifier que la lettre de "secreWord" n'aie pas été matché precedment
					if(word[i] == secretWord[j] && matched[j] == NONE)
					{
						match[i] = ISIN;
						matched[j] = ISIN;
					}	
				}
			}

		}

		printf("Match : \t%s  %d\n", match, wordLength - goodLetter);
	
		return 	wordLength - goodLetter;

	}
	else
	{
		if(!isInDico)printf("Mot incorrecte, n'existe pas dans le dictionnaire\n");
		if(!isRightSize)printf("Longeure incorrecte, veuillez entrer un mot de %d lettres\n", wordLength);
		return -1;
	}
}

/**
 * \fn int getWordDico(char * w)
 * \brief write a word read in file \a dico into \a w
 * \param \a w points to an already allocated char[]
 * \param \a dico the name of the file of dictionnary type, i.e. a list of words and the number of words at the beginning
 * \return the number of characters copied to \a w. -1 if an error occured.
 * \warning if the entry is too long or the file can not be openned, an error may be raised.
 */

int getWordDico(const char* dico, char * w) 
{
	FILE* file = NULL;
	unsigned long tailleDico = 0;
	file = fopen(dico, "r");

	if (file != NULL)
	{
		rewind(file);
		char car;
		do{			// mesurer la taille du dico
			car = fgetc(file);
			if(car == '\n') tailleDico++;
		}while(car != EOF);
		tailleDico++;
		rewind(file);//revenir au debut du fichier
		
		
		unsigned long idMot = rand() % tailleDico + 1; // sellectionner un mot au hasard
		
		for(long numVille = 0; numVille < idMot; numVille++) //.. et aller le chercher dans le fichier
		{
			fgets(w, BUFMAX-1, file);
		}
		
		int lenTmp = strlen(w);
		for(int i = 0; i<lenTmp;i++)//normaliser son ecriture "lemot\0" (retirer les retour chariots et retour a la ligne
		{
			if(w[i] == '\n' || w[i] == '\r' || w[i] == '\0')
			{
				w[i] = '\0';
				i = lenTmp;// sortir de la boucle des que l'on mets un \0 
			}
		}

		//lenTmp = strlen(w);
		//printf("|%s| len %d",w, lenTmp);

		fclose(file); // On ferme le fichier qui a été ouvert
		return strlen(w);
	}
	else 
	{
		printf("Impossible d'ouvrir le fichier : %s\n", dico);
		return -1;
	}
	
}


/**
 * @fn int addDico(const char* const word, const char* const dicFileName)
 * @brief add a word into the dictionnary if it is not present
 * @param \a word the word to add
 * @param \a dicFileName the name of the file
 * @return 0 if the word has been added and another value otherwise
 * @warning The word is added at the end of the file
 */

int addDico(const char* word, const char * dicFileName) 
{
	if(!inDico(word, dicFileName))// si le mot n'existe pas dans le dico on l'ajoute
	{
		/*
		char  wordTmp[BUFMAX]; char car = '\n';
		strcpy(wordTmp,word);
		strncat(wordTmp,&car,1);	
		writeLineAppend(dicFileName, wordTmp);
		*/


		writeAlpha(dicFileName, word);
	}
		
	return 0;
}


// Mettre en minusculle un chaine de caractère "word" de longueur "len"
void lowerCase(char* word, const int len)
{
	for(int i = 0; i< len ; i++)
	{
		if(word[i]>= 'A' && word[i]<='Z')word[i]-= 'A'-'a';
	}
}



//ecrire une nouvelle ligne "string"(ne contient pas de '\n' ou '\r') à sa pisition alphabetique dans le fichier "nameFile"
int writeAlpha(const char* nameFile, const char* string)
{
	FILE* file = NULL;
	file = fopen(nameFile, "r");
	if(file)
	{
		long pos = 0;
		rewind(file);
		char car;	
		while(car != EOF)// parcourrir les lignes du dico
		{

			int idWord = 0;

			car = fgetc(file);
			printf("Car1 = %c\n", car);
			while(string[idWord] == car)// parcourrir les lettres identiques du debut du mot
			{
				car = fgetc(file);
				idWord++;
			}
			
			if(string[idWord] > car)// condition sur la première lettre différente
			{// si le mot est situé après dans l'alphabet ..
				do{
					car = fgetc(file);
	
				}while(car != '\n' && car != EOF);//.. on continue jsqu'au saut de ligne, ..
			}
			else // ..si non, on à trouvé la position où on veux inserrer notre mot
			{
				
				fseek(file, -(idWord+1), SEEK_CUR);// on revient au debut de la ligne..
				pos = ftell(file);           // .. et on enregistre la position
				
				car = EOF;// sortir de la boucle
			}

		}

		fclose(file);

		// on ajout un saut de ligne à la fin du mot..
		char word[BUFMAX];
		strcpy(word, string);
		char tmp[] = "\n";
		strcat(word, tmp);
		
		// ..puis on l'insere dans le fichier
		writeStrAppendMidle(nameFile , word, pos);
		
		return 1;//fin
	}
	else 
	{
		printf("Unable to open file %s\n", nameFile);
		return -1;
	}

}

//inserer une chaine de carectère "string" à la position "pos" dans un fichier "nameFile"
int writeStrAppendMidle(const char* nameFile, const char* string, long pos)
{
	FILE* file = NULL;
	file = fopen(nameFile, "r+");
   	
	if(file)
	{
		fseek(file, -pos , SEEK_END);
		unsigned long fSize =  ftell(file);
		
		//printf("Pos %ld, size %ld,  lsize %ld\n",pos, fSize1, fSize);

		char fileS[fSize+1];

		fseek(file, pos, SEEK_SET);
		
		for(long i = 0; i<fSize; i++)
		{
			fileS[i] = fgetc(file);
		}
		fileS[fSize] = '\0';

		fseek(file, pos, SEEK_SET);
		
		fputs(string, file);
		fputs(fileS, file);
		
		fclose(file);
		return 1;
	}
	else return -1;
		
}


//ecrire une nouvelle ligne "string" à ligne "pos"  du fhichier "nameFile" 
int writeLineAppendMidle(const char* nameFile, const char* string, long pos)
{
	FILE* file = NULL;
	file = fopen(nameFile, "r+");
   	
	if(file)
	{
		
		fseek(file, 0, SEEK_END);
		unsigned long fSize =  ftell(file);
	
		char fileS[fSize];


		rewind(file);
		//char car;
		long id = 0;
		
		for(long i = 0; i<fSize; i++)
		{
			fileS[i] = fgetc(file);
		}

		for(long i = 0; i < pos; id++)
		{
			if(fileS[id] == '\n')i++;
		}
		
		
		fseek(file, id, SEEK_SET);
		fputs(string, file);
		fputc('\n', file);
		fputs(fileS+id, file);
		
		fclose(file);
		return 1;
	}
	else return -1;
		
}

//ecrire une nouvelle ligne "string" à la fin du fichier "nameFile"
int writeLineAppend(const char* nameFile, const char* string)
{
	FILE* file = NULL;
	file = fopen(nameFile, "a");
	if(file)
	{
		fputs(string, file);
		fclose(file);
		return 1;
	}
	else return -1;
		
}

//recuperer un chaine de caractère de la part de l'utilisateur (limitée en longueur)
int getStr(char * s, const int lmax) {
	int l=0;
	int c;
	do {
		c = getchar();	/* length is incremented after the copy */
		if(l <lmax-1 || (l==lmax-1 && c == '\n')) {
			s[l++]=c;
		}
	}	while(c != EOF && c != '\n');
	if(l>1)	--l;
	if(s[l] == EOF) { return -2;}		/* length is decremented before the comparison */
	if(s[l] != '\n')	{ return -1;}
	s[l]='\0';
	return l;
}
