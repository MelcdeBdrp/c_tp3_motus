/**

TP_Motus  Melchior de Beaudrap

 * \file main.c
 * \brief Motus to complete
 * \author Thierry Garaix garaix@emse.fr
 * \version 0.1
 * \date 10 november 2019
 *
 * Program for the game Motus where a secret word has to be found after a limited number of propositions.
 * After each proposition the letters at the same position as in the secret word are kept at their position.
 * Letters present in the proposed word and the secret word but at the wrong position are marked.
 * The game is lost if the word proposed does not exists and/or if the limited number of popositions is exceeded.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include "motus_lib.h"



int main(int argc, char **argv)
{
	
	sParam params; // = {3, 0, 4, 2, 4, "mydicdos.dic"};// valeurs par defaut si non passées en paramètres
	params.maxTry = 3;
	params.mode = 1;
	params.nbTours = 4;// inutilisé depuis l'implementation des boulles noires
	params.lMin = 3;
	params.lMax = 5;
	strcpy(params.pathDico, "mydicdos.dic");

	for(int i =0; i< argc; i++)// lecture des paramètres d'entrée
	{
		
		if( (!strcmp(argv[i], argDicoPathS) || !strcmp(argv[i], argDicoPath)) && i< argc - 1) 
		{
			//params.mode = 1;
			strcpy(params.pathDico, argv[i+1]);
		}
		if( (!strcmp(argv[i], argModeS) || !strcmp(argv[i], argMode)) && i< argc - 1)
		{
			params.mode = atoi(argv[i+1]);
		}
		if( (!strcmp(argv[i], argMaxTryS) || !strcmp(argv[i], argMaxTry)) && i< argc - 1)// si le paramètre est du type argMaxTryS ou argMaxTry, alors :
		{
			params.maxTry = atoi(argv[i+1]);
		}
	}
	
	//char* path = "dico.dic";
	//char* w = "abattbac";

	//writeAlpha(path, w);
	playMotus(&params);


	printf("\nMatch END\n\n");
	
	return 0;
}

