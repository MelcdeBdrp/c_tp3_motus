/**

TP_Motus  Melchior de Beaudrap

 * \file lib_motus.h
 *
 * Program for the game Motus where a secret word has to be found after a limited number of propositions.
 * After each proposition the letters at the same position as in the secret word are kept at their position.
 * Letters present in the proposed word and the secret word but at the wrong position are marked.
 * The game is lost if the word proposed does not exists and/or if the limited number of popositions is exceeded.
 */

#ifndef MOTUS_H
#define MOTUS_H


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>


#define BUFMAX 256		//<! Maximum length read from the instream buffer
#define MATCH '+' 
#define ISIN '/'
#define NONE ' '


// Q10A repond à la question 10.a)
//1 un mot incompatible fait perdre la main
//0 un mot incompatible ne coute rien
#define Q10A 1 



extern const char argMode[]; 
extern const char argMaxTry[]; 
extern const char argDicoPath[]; 

extern const char argModeS[]; 
extern const char argMaxTryS[]; 
extern const char argDicoPathS[]; 


/**
 * \struct sTeam
 */

typedef struct
{
	int toursG;
	int toursJ;
	int tMat;
	char name[100];
	int** mat;
	int nbNoires;
	int nbNc;
} sTeam;



/**
 * \struct sParam
 * \brief List of parameters of Motus
 */

typedef struct 
{
	int maxTry;
	int mode;	
	int nbTours;
	int lMin;
	int lMax;
	char pathDico[100];
	sTeam teams[2];
} sParam;



void initTeam(sTeam* team, int tMat);
void printMat(sTeam* team);
int pickBall(sTeam* team);
int victoire(sTeam* team);

void playMotus(sParam* params);
int tourMotus(const sParam* params);


void lowerCase(char* word, int len);
int writeLineAppend(const char*  nameFile, const char* string);
int writeAlpha(const char* nameFile, const char* string);
int writeLineAppendMidle(const char* nameFile, const char* string, long pos);
int writeStrAppendMidle(const char* nameFile, const char* string, long pos);


int getWord(char * w);
int inDico(const char* word, const char * dico);
int proposeWord(const char * secretWord, int wordLength, const char* dico);
int getWordDico(const char* dico, char * w);
int addDico(const char* word, const char * dicFileName);


int getStr(char * s, const int lmax) ;


#endif //MOTUS_H
