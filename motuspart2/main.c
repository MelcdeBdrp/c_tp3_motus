/**
 * \file main.c
 * \brief Motus to complete
 * \author Thierry Garaix garaix@emse.fr
 * \version 0.1
 * \date 10 november 2019
 *
 * Program for the game Motus where a secret word has to be found after a limited number of propositions.
 * After each proposition the letters at the same position as in the secret word are kept at their position.
 * Letters present in the proposed word and the secret word but at the wrong position are marked.
 * The game is lost if the word proposed does not exists and/or if the limited number of popositions is exceeded.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <sys/time.h>
#include "myutils.h" 

#define BUFMAX 256		//<! Maximum length read from the instream buffer
#define MATCH '+'
#define ISIN '/'
#define NONE ' '

void lowerCase(char* word, int len);
int writeLineAppend(const char* const nameFile, const char* const string);


/**
 * \fn int getWord(char * w)
 * \brief copy a word from \a stdin into \a w
 * \param \a w points to an already allocated char[]
 * \return the number of characters copied to \a w. -1 if an error occured.
 * \warning if the entry is too long an error may be raised.
 */
int getWord(char * w) {
	printf("\nGive a secret word: ");
	
	getStr(w, BUFMAX);
	int len = strlen(w);
	lowerCase(w, len);
	printf("%c[2J", 0x1B);	/* clear the screen */
	return len;
}


/** 
 * @fn int inDico(const char* const word, const char * const dicFileName)
 * @brief check if a word is in a dictionnary
 * @param \a word the word to find
 * @param \a dicFileName the name of the file
 * @return 1 if the word is in the file and 0 otherwise
 */
int inDico(const char* const word, const char * const dico) 
{
	FILE* file = NULL;
	unsigned long tailleDico = 0;
	file = fopen(dico, "r");
	int isInDico = 0;

	if (file != NULL)
	{
		rewind(file);
		char car;
		do{
			car = fgetc(file);
			if(car == '\n') tailleDico++;
		}while(car != EOF);
		tailleDico++;
		rewind(file);
		char w[BUFMAX];	
		
		for(long numVille = 0; numVille < tailleDico && !isInDico; numVille++)     //while(fgets(buff, 290, file) && playOnce)
		{
			fgets(w, BUFMAX-1, file);
			int lenTmp = strlen(w);
			for(int i = 0; i<lenTmp;i++)
			{
				if(w[i] == '\n' || w[i] == '\r' || w[i] == '\0')
				{
					w[i] = '\0';
					i = lenTmp;
				}
			}
			if(!strcmp(w, word))
			{
				isInDico = 1;
			}
		}

		

		//lenTmp = strlen(w);
		//printf("|%s| len %d",w, lenTmp);

		fclose(file); // On ferme le fichier qui a été ouvert
		return isInDico;
	}
	else 
	{
		printf("Impossible d'ouvrir le fichier : %s\n", dico);
		return -1;
	}
	

	return 0;	
}


/** 
 * @fn int proposeWord(const char * const secretWord)
 * @brief get a word from \a stdin and compare with the secretWord
 * @param secretWord the word to find out
 * @return the number of differencies
 */

int proposeWord(const char * const secretWord, int wordLength, const char* const dico) 
{
	printf("\nNew try: ");
	char word[wordLength+3], match[wordLength+1], matched[wordLength+1];// match correspond aux match indexé sur le mot proposé, matched indexé sur le mot mystere
	int goodLetter = 0;
	int isInDico = 0, isRightSize = 0;
	getStr(word, wordLength+2);
	lowerCase(word,wordLength);
	
	isInDico = inDico(word, dico);
	if(strlen(word) == wordLength)isRightSize = 1;
	
	match[wordLength] = '\0';
	matched[wordLength] = '\0';

	if(isInDico && isRightSize)
	{
		for(int i = 0; i< wordLength; i++)
		{
			if(secretWord[i] == word[i])
			{
				goodLetter++;
				match[i] = MATCH;
				matched[i] = MATCH;
			}
			else 
			{
				match[i] = NONE;
				matched[i] = NONE;
			}
		}

		for(int i = 0; i<wordLength; i++)
		{
			if(match[i] == NONE) 
			{
				for(int j = 0; j<wordLength; j++)
				{
					if(word[i] == secretWord[j] && matched[j] == NONE)
					{
						match[i] = ISIN;
						matched[j] = ISIN;
					}	
				}
			}

		}

		printf("          %s  %d\n", match, wordLength - goodLetter);
	
		return 	wordLength - goodLetter;

	}
	else
	{
		if(!isInDico)printf("Mot incorrecte, n'existe pas dans le dictionnaire\n");
		if(!isRightSize)printf("Longeure incorrecte, veuillez entrer un mot de %d lettres\n", wordLength);
		return -1;
	}
}

/**
 * \fn int getWordDico(char * w)
 * \brief write a word read in file \a dico into \a w
 * \param \a w points to an already allocated char[]
 * \param \a dico the name of the file of dictionnary type, i.e. a list of words and the number of words at the beginning
 * \return the number of characters copied to \a w. -1 if an error occured.
 * \warning if the entry is too long or the file can not be openned, an error may be raised.
 */
int getWordDico(char* dico, char * w) 
{
	FILE* file = NULL;
	unsigned long tailleDico = 0;
	file = fopen(dico, "r");

	if (file != NULL)
	{
		rewind(file);
		char car;
		do{
			car = fgetc(file);
			if(car == '\n') tailleDico++;
		}while(car != EOF);
		tailleDico++;
		rewind(file);
		
		
		unsigned long idMot = rand() % tailleDico + 1;
		printf("\nChosen word number %ld over %ld\n",idMot, tailleDico);
		for(long numVille = 0; numVille < idMot; numVille++)     //while(fgets(buff, 290, file) && playOnce)
		{
			fgets(w, BUFMAX-1, file);
		}
		
		int lenTmp = strlen(w);
		for(int i = 0; i<lenTmp;i++)
		{
			if(w[i] == '\n' || w[i] == '\r' || w[i] == '\0')
			{
				w[i] = '\0';
				i = lenTmp;
			}
		}

		//lenTmp = strlen(w);
		//printf("|%s| len %d",w, lenTmp);

		fclose(file); // On ferme le fichier qui a été ouvert
		return strlen(w);
	}
	else 
	{
		printf("Impossible d'ouvrir le fichier : %s\n", dico);
		return -1;
	}
	
}


/** 
 * @fn int proposeWord(const char * const secretWord)
 * @brief get a word from \a stdin and compare with the secretWord
 * @param secretWord the word to find out
 * @return the number of differencies
 */
/*
int proposeWord(const char * const secretWord,const char * const dicoName) {

	
}
*/


/**
 * @fn int addDico(const char* const word, const char* const dicFileName)
 * @brief add a word into the dictionnary if it is not present
 * @param \a word the word to add
 * @param \a dicFileName the name of the file
 * @return 0 if the word has been added and another value otherwise
 * @warning The word is added at the end of the file
 */
int addDico(const char* const word, const char * const dicFileName) 
{
	if(!inDico(word, dicFileName))
	{
		char  wordTmp[BUFMAX]; char car = '\n';
		strcpy(wordTmp,word);
		strncat(wordTmp,&car,1);	
		writeLineAppend(dicFileName, wordTmp);
	}
		
	return 0;
}

int main(int argc, char **argv)
{
	
	printf("Start MOTUS\n");
	
	char w[BUFMAX];	/* secret word */
	int len = 0;
	int error=0;
	int maxTry=6;	/* maximum number of tries to find one word */
	int nTry=0;	/* current number of tries */
//	int res=-2;	
	int DICO = 0;
	char dicoName[BUFMAX] = "mydicdos.dic";

	if(DICO)
	{
	
		srand((unsigned) time(NULL));
		len = getWordDico(dicoName, w);
		printf("Word '%s', len %d\n", w, len);
	}
	else
	{
		len = getWord(w);
		//inDico(w, dicoName);
		addDico(w, dicoName);
		if(error<0)
			perror(" EXIT PROGRAM ");
	}
	
	
	printf("\n Le mot secret est %s   %d\n",w, len);

	error = len;
	for(nTry = 0; nTry < maxTry && error; nTry++)
	{
		error = proposeWord(w, len, dicoName);
		if(error == -1) nTry --;
	}
	if(error)
	{
		printf("\nYou have used all your %d try, the word was %s\n\n", maxTry, w);
	}
	else
	{
		printf("\nYou have found the word '%s' in %d over %d try", w, nTry, maxTry);
	}



	return 0;
}

void lowerCase(char* word, int len)
{
	for(int i = 0; i< len ; i++)
	{
		if(word[i]>= 'A' && word[i]<='Z')word[i]-= 'A'-'a';
	}
}


int writeLineAppend(const char* const nameFile, const char* const string)
{
	FILE* file = NULL;
	file = fopen(nameFile, "a");
	if(file)
	{
		fputs(string, file);
		fclose(file);
		return 1;
	}
	else return 0;
		
}



